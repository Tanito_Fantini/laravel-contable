<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('empresa', function(Blueprint $table) {
      $table->smallIncrements('id');
      $table->string('razon_social', 200);
      $table->string('nombre_fantasia', 200)->nullable();
      $table->date('inicio_actividad')->nullable();
      $table->string('direccion', 200)->nullable();
      $table->string('telefono', 200)->nullable();
      $table->string('email', 200)->unique()->nullable();
      $table->text('observaciones')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('empresa');
  }
}
