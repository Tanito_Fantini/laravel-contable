<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaContableTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('cuenta_contable', function(Blueprint $table) {
      $table->bigIncrements('id');
      $table->smallInteger('id_empresa')->unsigned();
      $table->bigInteger('id_cuenta_contable_padre')->unsigned();
      $table->string('codigo', 100);
      $table->string('nombre', 200);
      $table->text('descripcion')->nullable();
      $table->tinyInteger('orden')->unsigned();
      $table->enum('tipo', ['ACTIVO','PASIVO','PATRIMONIO NETO','INGRESOS','EGRESOS']);

      $table
        ->foreign('id_empresa')
        ->references('id')->on('empresa')
        ->onUpdate('restrict')
        ->onDelete('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('cuenta_contable');
  }
}
