<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAsientoContable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('item_asiento_contable', function(Blueprint $table) {
      $table->bigIncrements('id');
      $table->bigInteger('id_asiento_contable')->unsigned();
      $table->bigInteger('id_cuenta_contable')->unsigned();
      $table->decimal('monto', 8, 2)->unsigned();
      $table->enum('tipo', ['DEBE','HABER']);

      $table
        ->foreign('id_asiento_contable')
        ->references('id')->on('asiento_contable')
        ->onUpdate('restrict')
        ->onDelete('restrict');

      $table
        ->foreign('id_cuenta_contable')
        ->references('id')->on('cuenta_contable')
        ->onUpdate('restrict')
        ->onDelete('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('item_asiento_contable');
  }
}
