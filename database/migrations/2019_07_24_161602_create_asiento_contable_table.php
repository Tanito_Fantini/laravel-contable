<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsientoContableTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('asiento_contable', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->smallInteger('id_empresa')->unsigned();
      $table->date('fecha');
      $table->mediumInteger('numero')->unsigned();
      $table->text('observaciones')->nullable();
      $table->decimal('total', 8, 2)->unsigned();
      $table->timestamps();
      $table->softDeletes();

      $table
        ->foreign('id_empresa')
        ->references('id')->on('empresa')
        ->onUpdate('restrict')
        ->onDelete('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
      Schema::dropIfExists('asiento_contable');
  }
}
