<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Empresa::class, function(Faker $faker) {
  return [
    'razon_social'     => $faker->name,
    'nombre_fantasia'  => $faker->text(25),
    'inicio_actividad' => $faker->date($format = 'Y-m-d', $max = 'now'),
    'direccion'        => $faker->text(50),
  ];
});
