<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use App\Models\CuentaContable;

class Empresa extends Model {

  use SoftDeletes;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'empresa';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['inicio_actividad', 'razon_social', 'nombre_fantasia', 'direccion', 'telefono', 'email', 'observaciones'];

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [boot description]
   * 
   * @return [type] [description]
   */
  public static function boot() {
    parent::boot();

    self::creating(function($model) {
      Log::info('creating! - ' . $model->razon_social . ' - ' . $model->id);
    });

    self::created(function($model) {
      Log::info('created! - ' . $model->razon_social . ' - ' . $model->id);

      $cc = new CuentaContable();

      $cc->generarPlan($model->id);
    });
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
