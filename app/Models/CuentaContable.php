<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuentaContable extends Model {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'cuenta_contable';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['id_empresa', 'id_cuenta_contable_padre', 'codigo', 'nombre', 'descripcion', 'orden', 'tipo'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['id_empresa'];

   // ---------------------------------------------------------------------------------------------------------------------

    /**
     * [empresa description]
     *
     * @access public
     * @return Empresa
     */
    public function empresa() {
      return $this->belongsTo('App\Models\Empresa', 'id_empresa');
    }

    // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Obtener todos los items hijos y sus descendientes
   *
   * @access public
   * @param  integer $id_cuenta_contable_padre
   * @param  array   $items
   * @return void
   */
  public static function obtenerItemsHijos($id_cuenta_contable_padre = 0, &$items) {
    // dd( $id_cuenta_contable_padre );

    $hijos = CuentaContable::select(['id', 'id_cuenta_contable_padre', 'codigo', 'nombre', 'orden', 'tipo'])
              ->where('id_cuenta_contable_padre', $id_cuenta_contable_padre)
              ->orderBy('orden', 'asc')
              ->get();
    // dd( $hijos );

    if( $hijos->count() ) {
      foreach( $hijos as $cc ) {
        $items[] = array(
          'id'                       => (int)$cc->id,
          'id_cuenta_contable_padre' => (int)$cc->id_cuenta_contable_padre,
          'codigo'                   => $cc->codigo,
          'nombre'                   => $cc->nombre,
          'orden'                    => $cc->orden,
          'tipo'                     => $cc->tipo
        );

        CuentaContable::obtenerItemsHijos((int)$cc->id, $items);
      }
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [generarPlan description]
   *
   * @access public
   * @param  integer $id_empresa [description]
   * @return [type]              [description]
   */
  public function generarPlan($id_empresa = 0) {
    $plan_cuentas = [
      [
        'codigo' => '1',
        'nombre' => 'ACTIVO',
        'items'  => [
          [
            'codigo' => '1.1',
            'nombre' => 'ACTIVO CORRIENTE',
            'items'  => [
              [
                'codigo' => '1.1.1',
                'nombre' => 'DISPONIBILIDADES',
                'items'  => [
                  [
                    'codigo' => '1.1.1.1',
                    'nombre' => 'CAJAS',
                    'items'  => [
                      [
                        'codigo' => '1.1.1.1.1',
                        'nombre' => 'CAJA CENTRAL'
                      ],
                      [
                        'codigo' => '1.1.1.1.2',
                        'nombre' => 'CAJA CHICA'
                      ]
                    ]
                  ],
                  [
                    'codigo' => '1.1.1.2',
                    'nombre' => 'BANCOS',
                  ],
                  [
                    'codigo' => '1.1.1.3',
                    'nombre' => 'VALORES A DEPOSITAR',
                    'items'  => [
                      [
                        'codigo' => '1.1.1.3.1',
                        'nombre' => 'CHEQUES EN CARTERA'
                      ],
                      [
                        'codigo' => '1.1.1.3.2',
                        'nombre' => 'CHEQUES RECHAZADOS'
                      ],
                      [
                        'codigo' => '1.1.1.3.3',
                        'nombre' => 'CHEQUES DEPOSITADOS'
                      ]
                    ]
                  ]
                ]
              ],
              [
                'codigo' => '1.1.2',
                'nombre' => 'CRÉDITOS',
                'items'  => [
                  [
                    'codigo' => '1.1.2.1',
                    'nombre' => 'CUENTAS A COBRAR',
                    'items'  => [
                      [
                        'codigo' => '1.1.2.1.1',
                        'nombre' => 'CUENTAS CORRIENTES CLIENTES'
                      ]
                    ]
                  ],
                  [
                    'codigo' => '1.1.2.2',
                    'nombre' => 'CRÉDITOS FISCALES',
                    'items'  => [
                      [
                        'codigo' => '1.1.2.2.1',
                        'nombre' => 'IVA CRÉDITO FISCAL 10,5',
                      ],
                      [
                        'codigo' => '1.1.2.2.2',
                        'nombre' => 'IVA CRÉDITO FISCAL 21',
                      ],
                      [
                        'codigo' => '1.1.2.2.3',
                        'nombre' => 'IVA CRÉDITO FISCAL 27',
                      ],
                      [
                        'codigo' => '1.1.2.2.4',
                        'nombre' => 'RETENCIONES IIBB (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.5',
                        'nombre' => 'RETENCIONES IVA (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.6',
                        'nombre' => 'RETENCIONES GANANCIAS (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.7',
                        'nombre' => 'RETENCIONES SUSS (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.8',
                        'nombre' => 'PERCEPCIONES IIBB (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.9',
                        'nombre' => 'PERCEPCIONES IVA (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.10',
                        'nombre' => 'PERCEPCIONES GANANCIAS (SUFRIDAS)',
                      ],
                      [
                        'codigo' => '1.1.2.2.11',
                        'nombre' => 'PERCEPCIONES SUSS (SUFRIDAS)',
                      ]
                    ]
                  ]
                ]
              ],
              [
                'codigo' => '1.1.3',
                'nombre' => 'BIENES DE CAMBIO',
                'items'  => [
                  [
                    'codigo' => '1.1.3.1',
                    'nombre' => 'BIENES CAMBIO POR VENTAS',
                    'items'  => [
                      [
                        'codigo' => '1.1.3.1.1',
                        'nombre' => 'MERCADERIAS'
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ],
          [
            'codigo' => '1.2',
            'nombre' => 'ACTIVO NO CORRIENTE',
            'items'  => [
              [
                'codigo' => '1.2.1',
                'nombre' => 'BIENES DE USO',
                'items'  => [
                  [
                    'codigo' => '1.2.1.1',
                    'nombre' => 'MUEBLES Y ÚTILES'
                  ],
                  [
                    'codigo' => '1.2.1.2',
                    'nombre' => 'RODADOS'
                  ],
                  [
                    'codigo' => '1.2.1.3',
                    'nombre' => 'INMUEBLES OFICINA'
                  ],
                  [
                    'codigo' => '1.2.1.4',
                    'nombre' => 'MÁQUINAS Y HERRAMIENTAS'
                  ],
                  [
                    'codigo' => '1.2.1.5',
                    'nombre' => 'EQUIPOS DE COMPUTACIÓN'
                  ]
                ]
              ]
            ]
          ]
        ]
      ],
      [
        'codigo' => '2',
        'nombre' => 'PASIVO',
        'items'  => [
          [
            'codigo' => '2.1',
            'nombre' => 'PASIVO CORRIENTE',
            'items'  => [
              [
                'codigo' => '2.1.1',
                'nombre' => 'DEUDAS COMERCIALES',
                'items'  => [
                  [
                    'codigo' => '2.1.1.1',
                    'nombre' => 'CUENTAS A PAGAR',
                    'items'  => [
                      [
                        'codigo' => '2.1.1.1.1',
                        'nombre' => 'CUENTAS CORRIENTES PROVEEDORES'
                      ]
                    ]
                  ]
                ]
              ],
              [
                'codigo' => '2.1.2',
                'nombre' => 'DEUDAS BANCARIAS Y FINANCIERAS'
              ],
              [
                'codigo' => '2.1.3',
                'nombre' => 'SUELDOS Y CARGAS SOC. A PAGAR'
              ],
              [
                'codigo' => '2.1.4',
                'nombre' => 'DEUDAS IMPOSITIVAS',
                'items'  => [
                  [
                    'codigo' => '2.1.4.1',
                    'nombre' => 'IVA DÉBITO FISCAL 10,5'
                  ],
                  [
                    'codigo' => '2.1.4.2',
                    'nombre' => 'IVA DÉBITO FISCAL 21'
                  ],
                  [
                    'codigo' => '2.1.4.3',
                    'nombre' => 'IVA DÉBITO FISCAL 27'
                  ],
                  [
                    'codigo' => '2.1.4.4',
                    'nombre' => 'RETENCIONES IIBB (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.5',
                    'nombre' => 'RETENCIONES IVA (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.6',
                    'nombre' => 'RETENCIONES GANANCIAS (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.7',
                    'nombre' => 'RETENCIONES SUSS (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.8',
                    'nombre' => 'PERCEPCIONES IIBB (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.9',
                    'nombre' => 'PERCEPCIONES IVA (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.10',
                    'nombre' => 'PERCEPCIONES GANANCIAS (EFECTUADAS)'
                  ],
                  [
                    'codigo' => '2.1.4.11',
                    'nombre' => 'PERCEPCIONES SUSS (EFECTUADAS)'
                  ]
                ]
              ]
            ]
          ],
          [
            'codigo' => '2.2',
            'nombre' => 'PASIVO NO CORRIENTE'
          ]
        ]
      ],
      [
        'codigo' => '3',
        'nombre' => 'PATRIMONIO NETO',
        'items'  => [
          [
            'codigo' => '3.1',
            'nombre' => 'CAPITAL'
          ]
        ]
      ],
      [
        'codigo' => '4',
        'nombre' => 'INGRESOS',
        'items'  => [
          [
            'codigo' => '4.1',
            'nombre' => 'POR VENTAS',
            'items'  => [
              [
                'codigo' => '4.1.1',
                'nombre' => 'VENTAS DE MERCADERIAS'
              ]
            ]
          ],
          [
            'codigo' => '4.2',
            'nombre' => 'OTROS INGRESOS',
            'items'  => [
              [
                'codigo' => '4.2.1',
                'nombre' => 'INTERESES GANADOS'
              ],
              [
                'codigo' => '4.2.2',
                'nombre' => 'INGRESOS VARIOS'
              ]
            ]
          ],
          [
            'codigo' => '4.3',
            'nombre' => 'BONIFICACIÓN COMPRA'
          ]
        ]
      ],
      [
        'codigo' => '5',
        'nombre' => 'EGRESOS',
        'items'  => [
          [
            'codigo' => '5.1',
            'nombre' => 'POR COMPRAS',
            'items'  => [
              [
                'codigo' => '5.1.1',
                'nombre' => 'COMPRAS DE MERCADERÍAS'
              ],
              [
                'codigo' => '5.1.2',
                'nombre' => 'INTERESES PERDIDOS'
              ]
            ]
          ],
          [
            'codigo' => '5.2',
            'nombre' => 'COSTOS',
            'items'  => [
              [
                'codigo' => '5.2.1',
                'nombre' => 'COSTO MERCADERÍAS VENDIDAS'
              ]
            ]
          ],
          [
            'codigo' => '5.3',
            'nombre' => 'GASTOS COMERCIO',
            'items'  => [
              [
                'codigo' => '5.3.1',
                'nombre' => 'SUELDOS Y JORNALES'
              ],
              [
                'codigo' => '5.3.2',
                'nombre' => 'CARGAS SOCIALES'
              ],
              [
                'codigo' => '5.3.3',
                'nombre' => 'HONORARIOS'
              ],
              [
                'codigo' => '5.3.4',
                'nombre' => 'ENERGÍA ELÉCTRICA'
              ],
              [
                'codigo' => '5.3.5',
                'nombre' => 'GAS'
              ],
              [
                'codigo' => '5.3.6',
                'nombre' => 'TELÉFONO'
              ],
              [
                'codigo' => '5.3.7',
                'nombre' => 'PAPELERÍA Y ÚTILES'
              ],
              [
                'codigo' => '5.3.8',
                'nombre' => 'PUBLICIDAD Y PROPAGANDA'
              ],
              [
                'codigo' => '5.3.9',
                'nombre' => 'TASAS MUNICIPALES'
              ],
              [
                'codigo' => '5.3.10',
                'nombre' => 'GASTOS GENERALES'
              ]
            ]
          ],
          [
            'codigo' => '5.4',
            'nombre' => 'GASTOS BANCARIOS',
            'items'  => [
              [
                'codigo' => '5.4.1',
                'nombre' => 'COMISIÓN MANTENIMIENTO CUENTA'
              ],
              [
                'codigo' => '5.4.2',
                'nombre' => 'INTERESES BANCARIOS'
              ],
              [
                'codigo' => '5.4.3',
                'nombre' => 'IMPUESTO A DÉBITOS Y CRÉDITOS'
              ]
            ]
          ],
          [
            'codigo' => '5.5',
            'nombre' => 'GASTOS RODADOS',
            'items'  => [
              [
                'codigo' => '5.5.1',
                'nombre' => 'GASTOS COMBUSTIBLES Y LUBRICANTES'
              ],
              [
                'codigo' => '5.5.2',
                'nombre' => 'GASTOS REPARACIONES RODADOS'
              ],
              [
                'codigo' => '5.5.3',
                'nombre' => 'GASTOS SEGUROS VEHÍCULOS'
              ]
            ]
          ],
          [
            'codigo' => '5.6',
            'nombre' => 'BONIFICACIÓN VENTA'
          ]
        ]
      ]
    ];

    $this->guardarItemsArbol($plan_cuentas, 0, $id_empresa);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [guardarItemsArbol description]
   *
   * @access public
   * @param  array  $items
   * @param  integer $id_cuenta_contable_padre
   * @param  integer $id_empresa
   * @return array
   */
  public function guardarItemsArbol($items = [], $id_cuenta_contable_padre = 0, $id_empresa = 0) {
    // dd( $items );

    $id_empresa = is_numeric($id_empresa) && $id_empresa > 0 ? $id_empresa : session('id_empresa_seleccionada');

    if( is_array($items) && ($cantidad=count($items)) > 0 ) {
      for( $i=0; $i<$cantidad; $i++ ) {
        $cc = $this->guardar([
          'id_empresa'               => $id_empresa,
          'id_cuenta_contable_padre' => $id_cuenta_contable_padre,
          'codigo'                   => $items[$i]['codigo'],
          'nombre'                   => $items[$i]['nombre']
        ]);

        // si tiene items hijos
        if( isset($items[$i]['items']) && is_array($items[$i]['items']) && count($items[$i]['items']) ) {
          $this->guardarItemsArbol($items[$i]['items'], $cc->id, $id_empresa);
        }
      }
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [guardar description]
   *
   * @access public
   * @param  array  $cuenta_contable
   * @return object
   */
  public static function guardar($cuenta_contable = []) {
    // cuando no se envio el numero de orden
    if( !isset($cuenta_contable['orden']) || !is_numeric($cuenta_contable['orden']) ) {
      $cc_orden = CuentaContable::select('orden')
                    ->where([
                      ['id_cuenta_contable_padre', '=',  $cuenta_contable['id_cuenta_contable_padre']],
                      ['id_empresa', '=',  $cuenta_contable['id_empresa']]
                    ])
                    ->orderBy('orden', 'desc')
                    ->first();
      // dd( $cc_orden );

      if( $cc_orden !== NULL ) {
        $cuenta_contable['orden'] = (int)$cc_orden->orden + 1;
      }
      else {
        $cuenta_contable['orden'] = 1;
      }
    }

    // si no se envio el tipo de item
    if( !isset($cuenta_contable['tipo']) ) {
      if( $cuenta_contable['id_cuenta_contable_padre'] == 0 ) {
        $cuenta_contable['tipo'] = $cuenta_contable['nombre'];
      }
      else {
        $cc_tipo = CuentaContable::select('tipo')
                    ->where([
                      ['id', '=',  $cuenta_contable['id_cuenta_contable_padre']],
                      ['id_empresa', '=',  $cuenta_contable['id_empresa']]
                    ])
                    ->first();

        if( $cc_tipo !== NULL ) {
          $cuenta_contable['tipo'] = $cc_tipo->tipo;
        }
      }
    }

    return CuentaContable::create($cuenta_contable);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Armar el arbol del Plan de cuentas
   *
   * @access public
   * @param  integer $id_cuenta_contable_padre
   * @param  string  $imprimir
   * @return string|array
   */
  public static function obtenerArbol($id_cuenta_contable_padre = 1, $imprimir = 'html') {
    if( $imprimir !== 'json' ) {
      $salida = '';
    }
    else {
      $salida = array();
    }

    $cc = CuentaContable::where([
              ['id_cuenta_contable_padre', '=', $id_cuenta_contable_padre]
            ])
            ->orderBy('orden', 'asc')
            ->get();

    if( $cc->count() ) {
      if( $imprimir !== 'json' ) {
        $salida .= '<ul>';
      }
      else {
        $i = 0;

        $salida = array();
      }

      foreach( $cc as $item ) {
        switch( $imprimir ) {
          case 'pdf':
            $salida .= '<li class="item">
                          <div class="contenido-item">
                            <table width="100%" border="0">
                              <tr>
                                <td width="70px" align="right">
                                  <div class="codigo-item">' . $item->codigo . '</div>
                                </td>
                                <td>
                                  <div class="nombre-item">' . $item->nombre . '</div>
                                </td>
                              </tr>
                            </table>
                          </div>';
            break;

          case 'html':
            $salida .= '<li class="item" data-id="' . $item->id . '" data-codigo="' . $item->codigo . '" data-orden="' . $item->orden . '" data-tipo="' . $item->tipo . '">
                          <div class="contenido-item">
                            <div class="btn-contraer-expandir no-select">(-)</div>
                            <div class="codigo-item">' . $item->codigo . '</div>
                            <div class="nombre-item">' . $item->nombre . '</div>
                            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
                              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                                +
                              </button>
                              <button type="button" class="btn btn-secondary btn-editar-item" title="editar la cuenta contable">
                                editar
                              </button>
                              <button type="button" class="btn btn-danger btn-eliminar-item" title="eliminar la cuenta contable y si los tuviera, sus descendientes">
                                x
                              </button>
                            </div>
                          </div>';
            break;

          case 'json':
            $salida[] = array(
              'codigo'      => $item->codigo,
              'nombre'      => $item->nombre,
              'descripcion' => $item->descripcion,
              'items'       => array()
            );

            $salida[$i]['items'] = CuentaContable::obtenerArbol((int)$item->id, $imprimir);

            if( count($salida[$i]['items']) === 0 ) {
              unset($salida[$i]['items']);
            }

            $i++;
            break;
        }

        if( $imprimir !== 'json' ) {
          $salida .= CuentaContable::obtenerArbol((int)$item->id, $imprimir);

          $salida .= '</li>';
        }
      }

      if( $imprimir !== 'json' ) {
        $salida .= '</ul>';
      }
    }

    return $salida;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [eliminar description]
   *
   * @access public
   * @param  integer $id_cuenta_contable
   * @return integer
   */
  public static function eliminar($id_cuenta_contable = 0) {
    $ids   = [];
    $hijos = [];

    CuentaContable::obtenerItemsHijos($id_cuenta_contable, $hijos);

    $ids[] = $id_cuenta_contable;

    // si se encontraron items hijos
    if( count($hijos) ) {
      foreach( $hijos as $cc ) {
        $ids[] = $cc['id'];
      }
    }

    return CuentaContable::destroy($ids);
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
