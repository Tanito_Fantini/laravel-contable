<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemAsientoContable extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'item_asiento_contable';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = false;

  /**
   * The relationships that should always be loaded.
   *
   * @var array
   */
  protected $with = ['cuenta_contable'];

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [asiento_contable description]
   *
   * @access public
   * @return AsientoContable
   */
  public function asiento_contable() {
    return $this->belongsTo('App\Models\AsientoContable', 'id_asiento_contable', 'id');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [phone description]
   *
   * @access public
   * @return CuentaContable
   */
  public function cuenta_contable() {
    return $this->hasOne('App\Models\CuentaContable', 'id_cuenta_contable', 'id');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [vaciar description]
   *
   * @access public
   * @param  integer $id_asiento_contable
   * @return [type]                       [description]
   */
  public static function vaciar($id_asiento_contable = 0) {
    return ItemAsientoContable::where('id_asiento_contable', (int)$id_asiento_contable)->delete();
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
