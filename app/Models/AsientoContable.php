<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ItemAsientoContable;
use Illuminate\Support\Facades\DB;

class AsientoContable extends Model {

  use SoftDeletes;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'asiento_contable';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [items description]
   *
   * @access public
   * @return [type] [description]
   */
  public function items() {
    return $this->hasMany('App\Models\ItemAsientoContable', 'id_asiento_contable', 'id');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [ultimoNumero description]
   *
   * @access public
   * @return integer
   */
  public static function ultimoNumero() {
    $numero = 1;

    $asiento = AsientoContable::select('numero')
                ->where('id_empresa', session('id_empresa_seleccionada'))
                ->orderBy('numero', 'desc')
                ->first();
    // dd( $asiento );

    if( $asiento !== null ) {
      $numero = (int)$asiento->numero + 1;
    }

    return $numero;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [guardar description]
   *
   * @access public
   * @param  array  $datos
   * @return AsientoContable
   */
  public function guardar($datos = []) {
    // dd( $datos );

    DB::transaction(function() use ($datos) {
      if( $this->id == NULL ) {
        $this->id_empresa = session('id_empresa_seleccionada');
        $this->numero     = AsientoContable::ultimoNumero();
      }

      $this->fecha         = fechaDB($datos['fecha']);
      $this->observaciones = $datos['observaciones'];
      $this->total         = $datos['total'];
      $this->save();

      ItemAsientoContable::vaciar($this->id);

      if( is_array($datos['items']) ) {
        foreach( $datos['items'] as $item ) {
          $i = new ItemAsientoContable();

          $i->id_cuenta_contable = $item['cuenta_contable'];
          $i->monto              = $item['monto'];
          $i->tipo               = $item['tipo'];

          $this->items()->save($i);
        }
      }
    });

    return $this;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [obtenerItems description]
   *
   * @access public
   * @return [type] [description]
   */
  public function obtenerItems() {
    $items = [];

    // dd( $this->id );

    $result = DB::table('item_asiento_contable AS i')
                ->select('i.id', 'i.monto', 'i.tipo', 'i.id_cuenta_contable', 'cc.codigo', 'cc.nombre', 'cc.tipo AS tipo_cuenta_contable')
                ->join('cuenta_contable AS cc', 'cc.id', '=', 'i.id_cuenta_contable')
                ->where(['i.id_asiento_contable' => $this->id])
                ->get();

    if( $result->count() ) {
      foreach( $result as $item ) {
        $items[] = [
          'id'              => (int)$item->id,
          'cuenta_contable' => [
            'id'     => (int)$item->id_cuenta_contable,
            'codigo' => $item->codigo,
            'nombre' => $item->nombre,
            'tipo'   => $item->tipo_cuenta_contable
          ],
          'monto'           => (float)$item->monto,
          'tipo'            => $item->tipo
        ];
      }
    }

    return $items;
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
