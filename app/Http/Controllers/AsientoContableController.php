<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AsientoContableRequest;
use App\Models\AsientoContable;
use App\Models\ItemAsientoContable;

class AsientoContableController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [index description]
   *
   * @access public
   * @param  [type] $id
   * @return void
   */
  public function index($id = null) {
    $asiento = AsientoContable::withTrashed()->where('id', $id)->first();
    // dd( $asiento );

    if( $asiento === null ) {
      $asiento = new AsientoContable();

      $asiento->id     = 0;
      $asiento->numero = AsientoContable::ultimoNumero();
      $asiento->fecha  = date('d/m/Y');
    }
    else {
      $asiento->fecha = formatoFecha($asiento->fecha);
    }

    $asiento->numero = str_pad($asiento->numero, 8, '0', STR_PAD_LEFT);
    $items           = $asiento->obtenerItems();

    return view('asiento_contable/index', compact('asiento', 'items'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [create description]
   *
   * @access public
   * @param  AsientoContableRequest $request
   * @return void
   */
  public function create(AsientoContableRequest $request) {
    // dd( $request );

    $asiento = new AsientoContable();

    $asiento->guardar($request->validated());
    // dd( $asiento );

    return redirect('asiento_contable/' . $asiento->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  AsientoContableRequest $request
   * @param  AsientoContable $asiento
   * @return void
   */
  public function update(AsientoContableRequest $request, AsientoContable $asiento) {
    // dd( $request );

    $asiento->guardar($request->validated());
    // dd( $asiento );

    return redirect('asiento_contable/' . $asiento->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [list description]
   *
   * @access public
   * @return void
   */
  public function listado() {
    $asientos = AsientoContable::withTrashed()->orderBy('numero', 'DESC')->paginate(10);
    // dd( $asientos );

    return view('asiento_contable/listado', compact('asientos'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  AsientoContable $asiento
   * @return void
   */
  public function eliminar(AsientoContable $asiento) {
    // dd( $asiento );

    $asiento->delete();

    return redirect('asiento_contable/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  integre $id
   * @return void
   */
  public function restaurar($id) {
    // dd( $id );

    $asiento = AsientoContable::withTrashed()->where('id', $id)->first();
    // dd( $asiento );

    if( $asiento !== NULL ) {
      $asiento->restore();
    }

    return redirect('asiento_contable/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
