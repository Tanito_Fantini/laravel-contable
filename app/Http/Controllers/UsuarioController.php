<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsuarioController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [index description]
   *
   * @access public
   * @param  [type] $id [description]
   * @return void
   */
  public function index($id = null) {
    // dd( $id );

    $usuario = User::where('id', $id)->first();
    // dd( $usuario );

    if( $usuario === null ) {
      $usuario = new User();

      $usuario->id = 0;
    }

    return view('usuario/index', compact('usuario'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Guardar los datos del nuevo usuario
   * 
   * @access public
   * @param  array $data
   * @return void
   */
  protected function create(Request $request) {
    // dd( $request );

    $validatedData = $request->validate([
      'email'    => 'required|string|email|max:255|unique:users',
      'name'     => 'required|string|max:255',
      'password' => 'required|string|min:8|confirmed'
    ]);
    // dd( $validatedData );

    $validatedData['password'] = Hash::make($validatedData['password']);

    $usuario = User::create($validatedData);

    return redirect('usuario/' . $usuario->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  Request $request
   * @param  User $usuario
   * @return void
   */
  public function update(Request $request, User $usuario) {
    // dd( $request );

    $rules = [
      'name' => 'required|string|max:255'
    ];

    if( $request->input('password') !== NULL ) {
      $rules['password'] = 'required|string|min:8|confirmed';
    }

    $validatedData = $request->validate($rules);
    // dd( $validatedData );

    $usuario->name = $validatedData['name'];

    if( $request->input('password') !== NULL ) {
      $usuario->password = Hash::make($validatedData['password']);
    }

    $usuario->save();

    return redirect('usuario/' . $usuario->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [list description]
   *
   * @access public
   * @return void
   */
  public function listado() {
    $usuarios = User::withTrashed()->orderBy('name', 'ASC')->paginate(10);
    // dd( $usuarios );

    return view('usuario/listado', compact('usuarios'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  User $usuario
   * @return void
   */
  public function eliminar(User $usuario) {
    // dd( $usuario );

    $usuario->delete();

    return redirect('usuario/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  integre $id
   * @return void
   */
  public function restaurar($id) {
    // dd( $id );

    $usuario = User::withTrashed()->where('id', $id)->first();
    // dd( $usuario );

    if( $usuario !== NULL ) {
      $usuario->restore();
    }

    return redirect('usuario/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [perfil description]
   *
   * @access public
   * @param  Request $request
   * @return void
   */
  public function perfil(Request $request) {
    // dd( $request->user() );

    $usuario = $request->user();

    return view('usuario/perfil', compact('usuario'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [guardarPerfil description]
   *
   * @access public
   * @param  Request $request
   * @return void
   */
  public function guardarPerfil(Request $request) {
    // dd( $request );

    $rules = [
      'name' => 'required|string|max:255'
    ];

    if( $request->input('password') !== NULL ) {
      $rules['password'] = 'required|string|min:8|confirmed';
    }

    $validatedData = $request->validate($rules);
    // dd( $validatedData );

    $usuario       = $request->user();
    $usuario->name = $validatedData['name'];

    if( $request->input('password') !== NULL ) {
      $usuario->password = Hash::make($validatedData['password']);
    }

    $usuario->save();

    return view('usuario/perfil', compact('usuario'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
