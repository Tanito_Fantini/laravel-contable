<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresa;

class EmpresaController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [index description]
   *
   * @access public
   * @param  [type] $id [description]
   * @return void
   */
  public function index($id = null) {
    // dd( $id );

    $empresa = Empresa::withTrashed()->where('id', $id)->first();
    // dd( $empresa );

    if( $empresa === null ) {
      $empresa = new Empresa();

      $empresa->id = 0;
    }
    else {
      $empresa->inicio_actividad = formatoFecha($empresa->inicio_actividad);
    }

    return view('empresa/index', compact('empresa'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [save description]
   *
   * @access public
   * @param  Request $request
   * @return void
   */
  public function create(Request $request) {
    // dd( $request );

    $validatedData = $request->validate([
      'inicio_actividad' => 'required|date_format:d/m/Y',
      'razon_social'     => 'required|max:200',
      'nombre_fantasia'  => 'max:200',
      'direccion'        => 'max:200',
      'telefono'         => 'max:200',
      'email'            => 'nullable|string|email|max:255|unique:empresa,email',
      'observaciones'    => 'max:250'
    ]);
    // dd( $validatedData );

    $validatedData['inicio_actividad'] = fechaDB($validatedData['inicio_actividad']);

    $empresa = Empresa::create($validatedData);

    return redirect('empresa/' . $empresa->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  Request $request
   * @param  Empresa $empresa
   * @return void
   */
  public function update(Request $request, Empresa $empresa) {
    // dd($request);

    $validatedData = $request->validate([
      'inicio_actividad' => 'required|date_format:d/m/Y',
      'razon_social'     => 'required|max:200',
      'nombre_fantasia'  => 'max:200',
      'direccion'        => 'max:200',
      'telefono'         => 'max:200',
      'email'            => 'nullable|string|email|max:255|unique:empresa,email,' . $empresa->id,
      'observaciones'    => 'max:250'
    ]);
    // dd( $validatedData );

    $empresa->inicio_actividad = fechaDB($validatedData['inicio_actividad']);
    $empresa->razon_social     = $validatedData['razon_social'];
    $empresa->nombre_fantasia  = $validatedData['nombre_fantasia'];
    $empresa->direccion        = $validatedData['direccion'];
    $empresa->telefono         = $validatedData['telefono'];
    $empresa->email            = $validatedData['email'];
    $empresa->observaciones    = $validatedData['observaciones'];

    $empresa->save();

    return redirect('empresa/' . $empresa->id);
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [list description]
   *
   * @access public
   * @return void
   */
  public function listado() {
    $empresas = Empresa::withTrashed()->orderBy('razon_social', 'ASC')->paginate(10);
    // dd( $empresas );

    return view('empresa/listado', compact('empresas'));
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  Empresa $empresa
   * @return void
   */
  public function eliminar(Empresa $empresa) {
    // dd( $empresa );

    $empresa->delete();

    return redirect('empresa/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [remove description]
   *
   * @access public
   * @param  integre $id
   * @return void
   */
  public function restaurar($id) {
    // dd( $id );

    $empresa = Empresa::withTrashed()->where('id', $id)->first();
    // dd( $empresa );

    if( $empresa !== NULL ) {
      $empresa->restore();
    }

    return redirect('empresa/listado');
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [all description]
   *
   * @access public
   * @param  Request $request
   * @return Empresa
   */
  public function all(Request $request) {
    $empresas = Empresa::orderBy('razon_social', 'asc')->get();

    if( $request->session()->get('id_empresa_seleccionada') ) {
      $empresas->each(function($item, $key) use ($request) {
        if( $item->id == $request->session()->get('id_empresa_seleccionada') ) {
          $item->seleccionada = true;

          return FALSE;
        }
      });
    }
    // dd( $empresas );

    return $empresas;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [seleccionar description]
   *
   * @access public
   * @return [type] [description]
   */
  public function seleccionar(Request $request, Empresa $empresa) {
    // dd( $empresa );

    $request->session()->put('id_empresa_seleccionada', $empresa->id);
    $request->session()->put('empresa', $empresa->razon_social);

    return $empresa;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [trabajo description]
   * 
   * @param  Request $request
   * @return [type]           [description]
   */
  public function trabajo(Request $request) {
    $empresa = Empresa::find($request->session()->get('id_empresa_seleccionada'));

    return $empresa !== null ? $empresa : [];
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
