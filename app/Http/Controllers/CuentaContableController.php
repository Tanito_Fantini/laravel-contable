<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CuentaContable;

class CuentaContableController extends Controller {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [create description]
   * 
   * @access public
   * @param  Request $request
   * @return void
   */
  public function create(Request $request) {
    // dd( $request );

    $validatedData = $request->validate([
      'id_cuenta_contable_padre' => 'required|integer',
      'codigo'                   => 'required|max:100',
      'nombre'                   => 'required|max:200',
      'descripcion'              => 'nullable|max:250',
      'orden'                    => 'required|integer',
      'tipo'                     => 'required|in:ACTIVO,PASIVO,PATRIMONIO NETO,INGRESOS,EGRESOS'
    ]);
    // dd( $validatedData );

    $validatedData['id_empresa'] = $request->session()->get('id_empresa_seleccionada');

    $cc = CuentaContable::guardar($validatedData);

    return $cc;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  Request $request
   * @param  CuentaContable $cuenta_contable
   * @return void
   */
  public function update(Request $request, CuentaContable $cuenta_contable) {
    // dd( $cuenta_contable );

    $validatedData = $request->validate([
      'nombre'      => 'required|max:200',
      'descripcion' => 'nullable|max:250'
    ]);
    // dd( $validatedData );

    $cuenta_contable->nombre      = $validatedData['nombre'];
    $cuenta_contable->descripcion = $validatedData['descripcion'];
    $cuenta_contable->save();

    return $cuenta_contable;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  Request $request
   * @param  CuentaContable $cuenta_contable
   * @return void
   */
  public function remove(Request $request, CuentaContable $cuenta_contable) {
    // dd( $cuenta_contable );

    return CuentaContable::eliminar( $cuenta_contable->id );
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [update description]
   *
   * @access public
   * @param  Request $request
   * @return void
   */
  public function select2(Request $request) {
    // dd( $request );

    $cuentas = CuentaContable::select('id', 'codigo', 'nombre', 'tipo')
                ->where('id_empresa', '=', $request->session()->get('id_empresa_seleccionada'))
                ->where(function($query) use ($request) {
                    $query->where('codigo', 'like', '%' . $request->input('term') . '%')
                          ->orWhere('nombre', 'like', '%' . $request->input('term') . '%');
                })
                ->limit(10)
                ->get();
    // dd( $cuentas );

    return $cuentas;
  }

  // ---------------------------------------------------------------------------------------------------------------------

}