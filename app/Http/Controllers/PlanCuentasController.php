<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CuentaContable;

class PlanCuentasController extends Controller {
  
  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * [index description]
   *
   * @access public
   * @param  Request $request
   * @return void
   */
  public function index(Request $request) {
    $cc_a = CuentaContable::select(['id', 'orden', 'tipo'])
      ->where([
        ['nombre', '=', 'ACTIVO'],
        ['id_empresa', '=', $request->session()->get('id_empresa_seleccionada')]
      ])
      ->first();
    // dd( $cc_a );

    $arbol_activo = CuentaContable::obtenerArbol($cc_a->id, 'html');
    // dd( $arbol_activo );

    $cc_p = CuentaContable::select(['id', 'orden', 'tipo'])
      ->where([
        ['nombre', '=', 'PASIVO'],
        ['id_empresa', '=', $request->session()->get('id_empresa_seleccionada')]
      ])
      ->first();
    // dd( $cc_p );

    $arbol_pasivo = CuentaContable::obtenerArbol($cc_p->id, 'html');
    // dd( $arbol_pasivo );

    $cc_pn = CuentaContable::select(['id', 'orden', 'tipo'])
      ->where([
        ['nombre', '=', 'PATRIMONIO NETO'],
        ['id_empresa', '=', $request->session()->get('id_empresa_seleccionada')]
      ])
      ->first();
    // dd( $cc_pn );

    $arbol_patrimonio_neto = CuentaContable::obtenerArbol($cc_pn->id, 'html');
    // dd( $arbol_patrimonio_neto );

    $cc_i = CuentaContable::select(['id', 'orden', 'tipo'])
      ->where([
        ['nombre', '=', 'INGRESOS'],
        ['id_empresa', '=', $request->session()->get('id_empresa_seleccionada')]
      ])
      ->first();
    // dd( $cc_i );

    $arbol_ingresos = CuentaContable::obtenerArbol($cc_i->id, 'html');
    // dd( $arbol_ingresos );

    $cc_e = CuentaContable::select(['id', 'orden', 'tipo'])
      ->where([
        ['nombre', '=', 'EGRESOS'],
        ['id_empresa', '=', $request->session()->get('id_empresa_seleccionada')]
      ])
      ->first();
    // dd( $cc_e );

    $arbol_egresos = CuentaContable::obtenerArbol($cc_e->id, 'html');
    // dd( $arbol_egresos );

    return view('plan_cuentas/index', [
      'activo'                => $cc_a,
      'arbol_activo'          => $arbol_activo,
      'pasivo'                => $cc_p,
      'arbol_pasivo'          => $arbol_pasivo,
      'patrimonio_neto'       => $cc_pn,
      'arbol_patrimonio_neto' => $arbol_patrimonio_neto,
      'ingresos'              => $cc_i,
      'arbol_ingresos'        => $arbol_ingresos,
      'egresos'               => $cc_e,
      'arbol_egresos'         => $arbol_egresos
    ]);
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
