<?php

namespace App\Http\Middleware;

use Closure;

class CheckEmpresa {
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    // dd( $request );

    if( $request->session()->get('id_empresa_seleccionada') === null ) {
      $request->session()->flash('error', 'Debe seleccionar una Empresa de trabajo.');

      return redirect('/');
    }

    return $next($request);
  }
}
