<?php

namespace App\Http\Middleware;

use Closure;

class checkAdmin {
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    // dd( $request->user() );

    if( $request->user()->id !== 1 ) {
      $request->session()->flash('error', 'Usted no puede acceder a las acciones solicitadas.');

      return redirect('/');
    }

    return $next($request);
  }
}
