<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AsientoContableRequest extends FormRequest {

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'fecha'         => 'required|date_format:d/m/Y',
      'observaciones' => 'max:200',
      'items'         => 'required',
      'total'         => 'required|numeric'
    ];
  }

  // ---------------------------------------------------------------------------------------------------------------------

}
