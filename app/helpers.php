<?php

if( !function_exists('fechaDB') ) {
  /**
   * Establecer el formato de
   * fecha utilizado por la base de datos
   * 
   * @param  string $fecha
   * @param  string $delimitador
   * @return string
   */
  function fechaDB($fecha = '', $delimitador = '/') {
    $salida = '';

    // si es una fecha sin un delimitador
    if( $delimitador === '' || $delimitador === NULL ) {
      $salida = substr($fecha, 4, 4) . '-' . substr($fecha, 2, 2) . '-' .substr($fecha, 0, 2);
    }
    else {
      // separamos la fecha en sus partes
      $fecha = explode($delimitador, $fecha);

      $salida = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
    }

    return $salida;
  }
}

// ---------------------------------------------------------------------------------------------------------------------

if( !function_exists('formatoFecha') ) {
  /**
   * Dar formato local a la fecha
   * 
   * @param  string  $fecha
   * @param  boolean $devolver_partes
   * @return string
   */
  function formatoFecha($fecha, $devolver_partes = FALSE) {
    if( $devolver_partes ) {
      $out = [];
    } else {
      $out = '';
    }

    // dump_exit( $fecha );

    if( is_string($fecha) ) {
      // si tiene formato yyyymmdd
      if( strlen($fecha) === 8 ) {
        if( $devolver_partes ) {
          $out = [substr($fecha, 6, 2), substr($fecha, 4, 2), substr($fecha, 0, 4)];
        } else {
          $out = substr($fecha, 6, 2) . '/' . substr($fecha, 4, 2) . '/' . substr($fecha, 0, 4);
        }
      }
      else
        // si tiene formato yyyy-mm-dd
        if( strlen($fecha) === 10 ) {
          $out = explode('-', $fecha);

          if( !$devolver_partes ) {
            $out = $out[2].'/'.$out[1].'/'.$out[0];
          }
        }
        else
          // si tiene formato yyyymmddhhmmss
          if( strlen($fecha) === 14 ) {
            if( !$devolver_partes ) {
              $out = substr($fecha, 7, 2) . '/' . substr($fecha, 4, 2) . '/' . substr($fecha, 0, 4);
              $out .= ' ' . substr($fecha, 8, 2) . ':' . substr($fecha, 10, 2) . ':' . substr($fecha, 12, 2);
            }
            else {
              $out[] = substr($fecha, 7, 2);
              $out[] = substr($fecha, 4, 2);
              $out[] = substr($fecha, 0, 4);
              $out[] = substr($fecha, 8, 2);
              $out[] = substr($fecha, 10, 2);
              $out[] = substr($fecha, 12, 2);
            }
          }
          else
            // si tiene formato yyyy-mm-dd hh:mm:ss
            if( strlen($fecha) === 19 ) {
              $partes = explode(' ', $fecha);

              if( !$devolver_partes ) {
                $fecha = explode('-', $partes[0]);
                $fecha = $fecha[2].'/'.$fecha[1].'/'.$fecha[0];

                $out = $fecha.' '.$partes[1];
              }
              else {
                $aux1 = explode('-', $partes[0]);
                $aux2 = explode(':', $partes[1]);

                $out[] = $aux1[2];
                $out[] = $aux1[1];
                $out[] = $aux1[0];
                $out   = array_merge($out, $aux2);
              }
            }
    }

    return $out;
  }
}

// ---------------------------------------------------------------------------------------------------------------------

if( !function_exists('formatoNumero') ) {
  /**
   * Formatear el numero
   * 
   * @param  float   $numero
   * @param  integer $decimales
   * @return string
   */
  function formatoNumero($numero, $decimales = NULL, $eliminar_ceros = FALSE) {
    // establecemos la cantidad de decimales
    $decimales = !is_numeric($decimales) ? config('contabilidad.max_decimales') : (int)$decimales;

    // establecemos el formato
    $numero = number_format($numero, $decimales, ',', '.');

    // si se deben eliminar los ceros a la derecha de la parte decimal
    if( $eliminar_ceros ) {
      // quitamos los ceros que tenga a la derecha
      $numero = rtrim($numero, '0');

      if( $numero[strlen($numero) - 1] == ',' ) {
        $numero = rtrim($numero, ',');
      }
    }

    return $numero;
  }
}

// ---------------------------------------------------------------------------------------------------------------------