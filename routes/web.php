<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
  'register' => false,
  'reset'    => false,
  'verify'   => false
]);

Route::middleware(['auth'])->group(function() {

  Route::get('/', 'EmpresaController@listado');
  Route::get('/empresas', 'EmpresaController@all');
  Route::get('/empresa/seleccionar/{empresa}', 'EmpresaController@seleccionar');
  Route::get('/empresa/listado', 'EmpresaController@listado');
  Route::post('/empresa/guardar', 'EmpresaController@create');
  Route::patch('/empresa/editar/{empresa}', 'EmpresaController@update');
  Route::get('/empresa/eliminar/{empresa}', 'EmpresaController@eliminar');
  Route::get('/empresa/restaurar/{id}', 'EmpresaController@restaurar');
  Route::get('/empresa/trabajo', 'EmpresaController@trabajo');

  Route::middleware(['check_empresa'])->group(function() {
    Route::get('/empresa/plan_cuentas', 'PlanCuentasController@index');

    Route::get('/asiento_contable', 'AsientoContableController@index');
    Route::post('/asiento_contable/guardar', 'AsientoContableController@create');
    Route::patch('/asiento_contable/editar/{asiento}', 'AsientoContableController@update');
    Route::get('/asiento_contable/listado', 'AsientoContableController@listado');

    Route::post('/cuenta_contable/guardar', 'CuentaContableController@create');
    Route::patch('/cuenta_contable/editar/{cuenta_contable}', 'CuentaContableController@update');
    Route::delete('/cuenta_contable/eliminar/{cuenta_contable}', 'CuentaContableController@remove');
    Route::get('/cuenta_contable/select2', 'CuentaContableController@select2');
  });

  Route::get('/usuario/perfil', 'UsuarioController@perfil');
  Route::post('/usuario/guardarPerfil', 'UsuarioController@guardarPerfil');

  Route::middleware(['check_admin'])->group(function() {
    Route::get('/usuario/listado', 'UsuarioController@listado');
    Route::post('/usuario/guardar', 'UsuarioController@create');
    Route::patch('/usuario/editar/{usuario}', 'UsuarioController@update');
    Route::get('/usuario/eliminar/{usuario}', 'UsuarioController@eliminar');
    Route::get('/usuario/restaurar/{id}', 'UsuarioController@restaurar');
    Route::get('/usuario/{id?}', 'UsuarioController@index');
  });

  Route::get('/empresa/{id?}', 'EmpresaController@index');
  Route::get('/asiento_contable/{id?}', 'AsientoContableController@index');

});


// Route::get('/home', 'HomeController@index')->name('home');


Route::get('create-users', function() {
  $users = factory(App\User::class, 30)->create();

  return $users;
});

Route::get('create-empresas', function() {
  $empresas = factory(App\Models\Empresa::class, 5)->create();

  return $empresas;
});