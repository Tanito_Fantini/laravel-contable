/*jshint esversion: 6 */

var appAsientoContable = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  const TIMEOUT_AJAX = 60000;

  var
      debug              = false,
      $window            = $(window),
      $document          = $(window.document),
      inicializado       = false,
      $fecha             = $("#fecha"),
      $debe              = $("#debe"),
      $haber             = $("#haber"),
      $diferencia        = $("#diferencia"),
      $tabla_items       = $("#tabla-items"),
      template_fila_item = Handlebars.compile($("#fila-item-template").html()),
      timeoutIDTotal     = null,
      $total             = $("#total"),

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      },

      /**
       * [description]
       *
       * @private
       * @param  {object} item
       * @return {void}
       */
      agregarItem = (item) => {
        $tabla_items
          .find("tbody")
          .eq(0)
          .append(template_fila_item());

        // referencia a la fila agregada
        $tr = $(".fila-item").last();

        $tr
          .find(".input-cuenta-contable")
          .eq(0)
          .select2({
            placeholder: "Seleccione una Cuenta Contable",
            language: "es",
            minimumInputLength: 2,
            maximumInputLength: 200,
            width: "100%",
            ajax: {
              url: "/cuenta_contable/select2",
              dataType: "json",
              delay: 250,
              processResults: (data) => {
                return {
                  results: data
                };
              },
            },
            templateResult: (data) => {
              var html;

              if( !data.id ) {
                return data.text;
              }

              html = `<table border="0" width="100%">
                        <tbody>
                          <tr>
                            <td><small>${data.codigo}</small> ${data.nombre}</td>
                          </tr>
                          <tr>
                            <td><small>${data.tipo}</small></td>
                          </tt>
                        </tbody>
                      </table>`;

              return $(html);
            },
            templateSelection: (data) => {
              if( !data.id ) {
                return data.text;
              }

              // log(data);

              calcularTotales();

              return data.text !== undefined && data.text !== "" ? $(data.text) : $(`<small>${data.codigo}</small> <span>${data.nombre}</span>`);
            }
          });

        new AutoNumeric($tr.find(".input-monto").eq(0)[0], {
          decimalCharacter : ',',
          digitGroupSeparator : '.',
        });

        $tr.find(".input-monto").eq(0)
          .on("keyup", (e) => {
            calcularTotales();
          });

        $tr
          .find(".input-tipo")
          .eq(0)
          .on("change", () => {
            calcularTotales();
          });

        $tr
          .find(".btn-eliminar")
          .eq(0)
          .on("click", (e) => {
            var
                $btn = $(e.currentTarget),
                $tr  = $btn.parents(".fila-item").eq(0);

            eliminarItem($tr[0]);
          })
          .on("keydown", (e) => {
            var
                $tr,
                $tr_next;

            // log( e.keyCode );

            // si se presiono la tecla TAB
            // y si no se presiona la tecla SHIFT
            if( e.keyCode === 9 && !e.shiftKey ) {
              e.preventDefault();

              $tr      = $(e.currentTarget).parents(".fila-item").eq(0);
              $tr_next = $tr.next();

              // si no tiene una fila adebajo
              if( $tr_next.length === 0 ) {
                agregarItem();
              }
              else {
                $tr_next
                  .find(".input-cuenta-contable")
                  .eq(0)
                  .select2("focus");
              }
            }
            else
              // si se presiono la tecla ESC
              if( e.keyCode === 27 ) {
                $("#btn-guardar").eq(0).focus();
              }
          });

        if( $.type(item) === "object" && !jQuery.isEmptyObject( item ) ) {
          // log(item);

          let cc = `<small>${item.cuenta_contable.codigo}</small> <span>${item.cuenta_contable.nombre}</span>`;

          // create the option and append to Select2
          let option = new Option(cc, item.cuenta_contable.id, true, true);

          $tr
            .find(".input-cuenta-contable")
            .eq(0)
            .append(option)
            .trigger("change");

          AutoNumeric.set($tr.find(".input-monto").eq(0)[0], item.monto);

          $tr
            .find(".input-tipo")
            .eq(0)
            .val(item.tipo);
        }
        else {
          // dar el foco al campo de la cuenta contable
          $tr
            .find(".input-cuenta-contable")
            .eq(0)
            .select2("open");
        }
      },

      /**
       * [description]
       *
       * @private
       * @param  {DOM element} tr
       * @return {void}
       */
      eliminarItem = (tr) => {
        $(tr).remove();

        calcularTotales();
      },

      /**
       * [description]
       * 
       * @return {void}
       */
      calcularTotales = () => {
        try {
          window.clearTimeout(timeoutIDTotal);
        }
        catch(e) {
        }

        timeoutIDTotal = window.setTimeout(() => {
          var
              debe  = 0,
              haber = 0;

          // recorremos las filas
          $(".fila-item").each((index, element) => {
            var
                $fila  = $(element),
                $tipo  = $fila.find(".input-tipo").eq(0),
                $monto = $fila.find(".input-monto").eq(0);

            if( $tipo.val() === "DEBE" ) {
              debe = debe + window.parseFloat(AutoNumeric.getAutoNumericElement($monto[0]).get());
            } else {
              haber = haber + window.parseFloat(AutoNumeric.getAutoNumericElement($monto[0]).get());
            }
          });

          // establecemos los totales
          AutoNumeric.set($debe[0], debe);
          AutoNumeric.set($haber[0], haber);
          AutoNumeric.set($diferencia[0], Math.abs(debe - haber));
        }, 100);
      },

      /**
       * Establecer los nombres de los items
       *
       * @private
       * @return {void}
       */
      acomodarItems = function() {
        // recorremos las filas
        $(".fila-item").each(function(index, element) {
          var
              $fila     = $(element),
              $input_cc = $fila.find(".input-cuenta-contable").eq(0);

          if( $input_cc.val() !== null ) {
            // cuenta contable
            $input_cc.attr("name", "items[" + index + "][cuenta_contable]");

            // monto
            $fila
              .find(".input-monto-oculto")
              .eq(0)
              .attr("name", "items[" + index + "][monto]")
              .val(AutoNumeric.getAutoNumericElement($fila.find(".input-monto").eq(0)[0]).get());

            // tipo
            $fila
              .find(".input-tipo")
              .eq(0)
              .attr("name", "items[" + index + "][tipo]");
          }
        });
      };


      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar al app
   *
   * @public
   * @return {void}
   */
  context.inicializar = function() {
    // si no se ha inicializado la app
    if( !inicializado ) {
      const opciones_numero = {
        decimalCharacter : ',',
        digitGroupSeparator : '.',
        decimalPlaces: 2,
        emptyInputBehavior: 'zero',
        leadingZero: 'deny',
        minimumValue: '0'
      };

      $fecha.datepicker({
        dateFormat: "dd/mm/yy"
      });

      new AutoNumeric($debe[0], opciones_numero);

      new AutoNumeric($haber[0], opciones_numero);

      new AutoNumeric($diferencia[0], opciones_numero);

      $("#btn-agregar-item").on("click", () => {
        agregarItem();
      });

      $("#formulario-asiento-contable").on("submit", (e) => {
        var error = false;

        if( $fecha.val() === "" ) {
          error = true;

          $fecha.addClass("is-invalid");
        }
        else {
          $fecha.removeClass("is-invalid");
        }

        if( window.parseFloat(AutoNumeric.getAutoNumericElement($diferencia[0]).get()) > 0 ) {
          error = true;

          $diferencia.addClass("is-invalid");
        }
        else {
          $diferencia.removeClass("is-invalid");
        }

        if( !error ) {
          acomodarItems();

          $total.val(AutoNumeric.getAutoNumericElement($debe[0]).get());

          $("#btn-guardar")
            .text("Guardando...")
            .prop("disabled", true);

          return true;
        }

        e.preventDefault();
      });

      $("#btn-listado").on("click", () => {
        window.location = '/asiento_contable/listado';
      });

      inicializado = true;
    }
    else {
      throw new Error("La app ya ha sido inicializada");
    }
  };

  /**
   * Cargar los items solicitadas
   *
   * @public
   * @param  {array} items
   * @return {void}
   */
  context.cargarItems = function(items) {
    if( $.type(items) === "array" ) {
      items.forEach((item) => {
        agregarItem(item);
      });
    }
  };

})( appAsientoContable, window );

// DOM ready!
$(() => {

  appAsientoContable.inicializar();

  appAsientoContable.cargarItems(items);

});