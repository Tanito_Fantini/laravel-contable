/*jshint esversion: 6 */

var appUsuario = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  var
      debug        = false,
      inicializado = false;

  const
      $window                = $(window),
      $document              = $(window.document),
      $id                    = $("#id"),
      $nombre                = $("#nombre"),
      $contrasenia           = $("#contrasenia"),
      $confirmar_contrasenia = $("#confirmar_contrasenia"),

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      };

      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar al app
   *
   * @public
   * @return {void}
   */
  context.inicializar = () => {
    // si no se ha inicializado la app
    if( !inicializado ) {
      $('[data-toggle="tooltip"]').tooltip();

      $nombre.select();

      // validacion del formulario
      $("#formulario-perfil").on("submit", (e) => {
        var
            error       = false,
            contrasenia = $.trim($contrasenia.val());

        if( $nombre.val() === "" ) {
          error = true;

          $nombre.addClass("is-invalid");
        }
        else {
          $nombre.removeClass("is-invalid");
        }

        if( contrasenia !== "" ) {
          if( contrasenia.length < 8 ) {
            error = true;

            $contrasenia.addClass("is-invalid");
          }
          else {
            $contrasenia.removeClass("is-invalid");

            if( $confirmar_contrasenia.val() !== contrasenia ) {
              error = true;

              $confirmar_contrasenia.addClass("is-invalid");
            }
            else {
              $confirmar_contrasenia.removeClass("is-invalid");
            }
          }
        }
        else {
          $contrasenia.removeClass("is-invalid");
          $confirmar_contrasenia.removeClass("is-invalid");
        }

        if( !error ) {
          $("#btn-guardar")
            .text("Guardando...")
            .prop("disabled", true);

          return true;
        }

        e.preventDefault();
      });

      inicializado = true;
    }
    else {
      throw new Error("La app ya ha sido inicializada");
    }
  };

})( appUsuario, window );

// DOM ready!
$(() => {

  appUsuario.inicializar();

});