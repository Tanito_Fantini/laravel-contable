/*jshint esversion: 6 */

var appListado = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  var
      debug        = false,
      $window      = $(window),
      $document    = $(window.document),
      inicializado = false,

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      };


      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar al app
   *
   * @public
   * @return {void}
   */
  context.inicializar = function() {
    // si no se ha inicializado la app
    if( !inicializado ) {

      $(".btn-editar").on("click", (e) => {
        var
            $btn = $(e.currentTarget),
            $info = $btn.parents("tr").eq(0).find(".info").eq(0);

        window.location = "/usuario/" + $info.data("id");
      });

      $(".btn-eliminar").on("click", (e) => {
        var
            $btn   = $(e.currentTarget),
            $tr    = $btn.parents("tr").eq(0),
            $info  = $tr.find(".info").eq(0),
            nombre = $.trim($tr.find("td").eq(1).text());

        if( window.confirm("¿Eliminar el usuario " + nombre + "?") ) {
          window.location = "/usuario/eliminar/" + $info.data("id");
        }
      });

      $(".btn-restaurar").on("click", (e) => {
        var
            $btn   = $(e.currentTarget),
            $tr    = $btn.parents("tr").eq(0),
            $info  = $tr.find(".info").eq(0),
            nombre = $.trim($tr.find("td").eq(1).text());

        if( window.confirm("¿Restaurar el usuario " + nombre + "?") ) {
          window.location = "/usuario/restaurar/" + $info.data("id");
        }
      });

      inicializado = true;
    }
    else {
      throw new Error("La app ya ha sido inicializada");
    }
  };

})( appListado, window );

// DOM ready!
$(() => {

  appListado.inicializar();

});