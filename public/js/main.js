/*jshint esversion: 6 */

var mainApp = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  const TIMEOUT_AJAX = 60000;

  var
      debug                      = false,
      $window                    = $(window),
      $document                  = $(window.document),
      inicializado               = false,
      mostrando_empresas         = false,
      xhr_ajax_empresas          = null,
      $modal_seleccion_empresas  = $("#modal-seleccion-empresas"),
      $img_obteniendo_empresas   = $("#img-obteniendo-empresas"),
      $tabla_seleccionar_empresa = $("#tabla-seleccionar-empresa"),

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      },

      /**
       * Obtener los datos de la empresa de trabajo
       *
       * @private
       * @param  {string} url
       * @return {void}
       */
      obtenerEmpresaTrabajo = (url) => {
        try {
          xhr_ajax_empresa.abort();
        }
        catch(e) {
        }

        // hacemos la peticion ajax
        xhr_ajax_empresa = $.ajax({
          url         : "/empresa/trabajo",
          async       : true,
          cache       : false,
          type        : "GET",
          contentType : "application/x-www-form-urlencoded",
          timeout     : TIMEOUT_AJAX,
          dataType    : "json",
          beforeSend  : (jqXHR, settings) => {
            // no hacer nada
          }
        })
        .fail((jqXHR, textStatus, errorThrown) => {
          if( textStatus !== "abort" ) {
            $.toast({
              heading: "Error",
              text: "Ocurrió un error al obtener la Empresa de trabajo. Inténtelo más tarde.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .done((data, textStatus, jqXHR) => {
          // log( data );

          if( $.type(data) === "object" && !jQuery.isEmptyObject(data) ) {
            $.toast({
              heading: data.razon_social,
              textAlign: "center",
              text: data.nombre_condicion_iva,
              position: "top-center",
              stack: 1,
              bgColor: "#75caeb",
              hideAfter: 1500
            });
          }
          else {
            $.toast({
              heading: "Error",
              text: "No se ha seleccionado la Empresa de trabajo aún.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .always(() => {
          // no hacer nada
        });
      },

      /**
       * Obtener y mostrar las empresas
       *
       * @private
       * @return {void}
       */
      obtenerEmpresas = () => {
        try {
          xhr_ajax_empresas.abort();
        }
        catch(e) {
        }

        // hacemos la peticion ajax
        xhr_ajax_empresas = $.ajax({
          url         : "/empresas",
          async       : true,
          cache       : false,
          type        : "GET",
          contentType : "application/x-www-form-urlencoded",
          timeout     : TIMEOUT_AJAX,
          dataType    : "json",
          beforeSend  : (jqXHR, settings) => {
            // mostramos el mensaje
            $img_obteniendo_empresas.removeClass("d-none");

            // limpiamos el contenido de la tabla
            $tabla_seleccionar_empresa
              .find("tbody")
              .eq(0)
              .empty();
          }
        })
        .fail((jqXHR, textStatus, errorThrown) => {
          if( textStatus !== "abort" ) {
            $.toast({
              heading: "Error",
              text: "Ocurrió un error al obtener las Empresas. Inténtelo más tarde.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .done((data, textStatus, jqXHR) => {
          var html = '';

          if( data.length ) {
            data.forEach((empresa) => {
              html += `<tr>
                         <td align="center">
                           ${empresa.inicio_actividad}
                         </td>
                         <td>
                           ${empresa.razon_social} ${empresa.seleccionada ? '<small>(seleccionada)</small>' : ''}
                         </td>
                         <td align="center">
                           <button type="button" class="btn btn-secondary btn-sm btn-seleccionar-empresa" data-id="${empresa.id}" ${empresa.seleccionada ? 'disabled' : ''}>
                             Seleccionar
                           </button>
                         </td>
                       </tr>`;
            });
          }
          else {
            html = '<tr><td colspan="3" align="center">No hay empresas cargadas</td></tr>';
          }

          // agregamos el contenido a al tabla
          $tabla_seleccionar_empresa
            .find("tbody")
            .eq(0)
            .html(html);
        })
        .always(() => {
          $img_obteniendo_empresas.addClass("d-none");
        });
      },

      /**
       * Establecer como "seleccionada"
       * la empresa solicitada
       *
       * @private
       * @param  {string} id
       * @return {void}
       */
      seleccionarEmpresa = (id) => {
        try {
          xhr_ajax_empresas.abort();
        }
        catch(e) {
        }

        // hacemos la peticion ajax
        xhr_ajax_empresas = $.ajax({
          url         : "/empresa/seleccionar/" + id,
          async       : true,
          cache       : false,
          type        : "GET",
          contentType : "application/x-www-form-urlencoded",
          timeout     : TIMEOUT_AJAX,
          dataType    : "json",
          beforeSend  : (jqXHR, settings) => {
            // deshabilitamos todos los botones
            $(".btn-seleccionar-empresa").prop("disabled", true);
          }
        })
        .fail((jqXHR, textStatus, errorThrown) => {
          if( textStatus !== "abort" ) {
            $.toast({
              heading: "Error",
              text: "Ocurrió un error al seleccionar la Empresa solicitada. Inténtelo más tarde.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .done((data, textStatus, jqXHR) => {
          // log( data );

          // ocultamos el modal
          $modal_seleccion_empresas.modal("hide");

          // refrescar la pagina
          window.location.reload();
        })
        .always(() => {
          // habilitamos todos los botones
          $(".btn-seleccionar-empresa").prop("disabled", false);
        });
      };

      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar
   *
   * @public
   * @return {void}
   */
  context.inicializar = () => {
    if( !inicializado ) {

      // al presionar una tecla
      $window.on("keydown", function(e) {
        // ctrl + shift + e
        if( e.ctrlKey && e.shiftKey && e.which === 69 ) {
          obtenerEmpresaTrabajo();
        }

        // ctrl + shift + s
        if( e.ctrlKey && e.shiftKey && e.which === 83 ) {
          mainApp.mostrarEmpresas();
        }
      });

      // it's fires immediately when the show instance method is called
      $modal_seleccion_empresas.on("show.bs.modal", (e) => {
        obtenerEmpresas();
      });

      // it's fired when the modal has finished being hidden from the user
      $modal_seleccion_empresas.on("hidden.bs.modal", (e) => {
        mostrando_empresas = false;
      });

      // al presionar el boton para seleccionar la empresa
      $tabla_seleccionar_empresa.on("click", ".btn-seleccionar-empresa", (e) => {
        var $btn = $(e.currentTarget);

        seleccionarEmpresa($btn.data("id"));
      });

      $(".btn-seleccionar-empresa").on("click", (e) => {
        e.preventDefault();

        mainApp.mostrarEmpresas();
      });

      inicializado = true;
    }
  };

  /**
   * Mostrar el listado de empresas
   * para seleccionar una de las mismas
   *
   * @public
   * @return {void}
   */
  context.mostrarEmpresas = () => {
    if( !mostrando_empresas ) {
      // hacemos visible el modal
      $modal_seleccion_empresas.modal({
        backdrop: "static",
        keyboard: false,
        show: true
      });

      mostrando_empresas = true;
    }
  };

})(mainApp, window);

// DOM ready!
$(() => {

  mainApp.inicializar();

});