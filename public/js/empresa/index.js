/*jshint esversion: 6 */

var appEmpresa = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  const TIMEOUT_AJAX = 60000;

  var
      debug             = false,
      $window           = $(window),
      $document         = $(window.document),
      inicializado      = false,
      $razon_social     = $("#razon_social"),
      $inicio_actividad = $("#inicio_actividad"),

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      };


      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar al app
   *
   * @public
   * @return {void}
   */
  context.inicializar = function() {
    // si no se ha inicializado la app
    if( !inicializado ) {
      $razon_social.select();

      $inicio_actividad.datepicker({
        dateFormat: "dd/mm/yy"
      });

      $("#formulario-empresa").on("submit", (e) => {
        var error = false;

        if( $.trim($razon_social.val()) === "" ) {
          error = true;

          $razon_social.addClass("is-invalid");
        }
        else {
          $razon_social.removeClass("is-invalid");
        }

        if( !error ) {
          $("#btn-guardar")
            .text("Guardando...")
            .prop("disabled", true);

          return true;
        }

        e.preventDefault();
      });

      $("#btn-listado").on("click", () => {
        window.location = '/empresa/listado';
      });

      inicializado = true;
    }
    else {
      throw new Error("La app ya ha sido inicializada");
    }
  };

})( appEmpresa, window );

// DOM ready!
$(() => {
  
  appEmpresa.inicializar();

});