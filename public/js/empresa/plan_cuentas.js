/*jshint esversion: 6 */

var appPlanCuentas = {};

((context, window) => {

      //---------------------------------
      // PRIVADO
      //---------------------------------

  const TIMEOUT_AJAX = 60000;

  var
      _debug                             = false,
      $window                            = $(window),
      $document                          = $(window.document),
      inicializado                       = false,
      $arbol                             = $("#arbol"),
      $modal_cuenta_contable             = $("#modal-cuenta-contable"),
      $titulo_modal                      = $("#titulo-modal"),
      $form_cuenta_contable              = $("#form-cuenta-contable"),
      $input_nombre_cuenta_contable      = $("#input-nombre-cuenta-contable"),
      $input_descripcion_cuenta_contable = $("#input-descripcion-cuenta-contable"),
      $btn_guardar_modal                 = $("#btn-guardar-modal"),
      $btn_cancelar_modal                = $("#btn-cancelar-modal"),
      item_template                      = Handlebars.compile($("#item-template").html()),
      li_aux_agregar_editar              = null,
      operacion                          = "",

      /**
       * Imprimir por consola
       *
       * @private
       * @param  {mixed} args
       * @return {void}
       */
      log = (...args) => {
        console.log( ...args );
      },

      /**
       * Acomodar los items del arbol
       *
       * @private
       * @return {void}
       */
      acomodarItems = () => {
        // recorremos los items del arbol
        $(".item").each(function(index, element) {
          var $item = $(element);

          // si no tiene items hijos
          if( $item.find("ul").length === 0 ) {
            $item.find(".btn-contraer-expandir").eq(0).html("&nbsp;");
          }
        });
      },

      /**
       * Agregar un nuevo item dentro del item solicitado
       *
       * @private
       * @param  {object} li
       * @param  {string} nombre
       * @param  {string} descripcion
       * @return {void}
       */
      agregarItem = function(li, nombre, descripcion) {
        var
            $li_padre = $(li),
            $li,
            orden;

        // log( $li_padre[0] );

        // creamos el item
        $li = $(document.createElement("li"))
                .addClass("item")
                .html(item_template({nombre: nombre}))
                .data({
                  id: 0, // no definido aun
                  orden: 0, // no definido aun
                  tipo: "" // no definido aun
                });

        // si el item padre no posee el contenedor de los items hijos
        if( $li_padre.find("ul").length === 0 ) {
          // agregamos el contenedor para los items hijos
          $li_padre.append("<ul></ul>");
        }

        // agregamos el item al contenedor
        $li_padre.find("ul").eq(0).append($li);

        // si es el primer item
        if( $li.prev().length === 0 ) {
          orden = 1;
        }
        // no es el primer item
        else {
          orden = window.parseInt($li.prev().data("orden")) + 1;
        }

        // establecemos el numero de orden
        $li.data("orden", orden);

        // establecemos el tipo
        $li.data("tipo", $li_padre.data("tipo"));

        // establecemos la descripcion
        $li.data("descripcion", descripcion);

        establecerCodigoItem($li[0]);

        $.ajax({
          url         : "/cuenta_contable/guardar",
          async       : true,
          cache       : false,
          type        : "POST",
          data        : {
            id                       : $li.data("id"),
            id_cuenta_contable_padre : $li_padre.data("id"),
            codigo                   : $li.data("codigo"),
            nombre                   : nombre,
            descripcion              : descripcion,
            orden                    : orden,
            tipo                     : $li.data("tipo"),
            "_token"                 : $('meta[name="csrf-token"]').attr("content")
          },
          contentType : "application/x-www-form-urlencoded",
          timeout     : TIMEOUT_AJAX,
          dataType    : "json",
          beforeSend  : function(jqXHR, settings) {
            // // mostramos el mensaje
            // appGeneral.mostrarMensajeBloqueante("Guardando Cuenta Contable");
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          if( textStatus !== "abort" ) {
            // eliminamos el item
            $li.remove();

            $.toast({
              heading: "Error",
              text: "Ocurrió un error al guardar la Cuenta Contable. Inténtelo más tarde.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .done(function(data, textStatus, jqXHR) {
          // log( data );return;

          // si es una cuenta contable nueva
          if( $li.data("id") == 0 ) {
            $li.data("id", data.id);

            $li.data("id-encoded", data.id_encoded);
          }

          // si el item padre no posee el boton para contraer/expandir
          if( $li_padre.find(".btn-contraer-expandir").eq(0).text() !== "(-)" ) {
            $li_padre.find(".btn-contraer-expandir").eq(0).text("(-)");
          }
        })
        .always(function() {
          // // ocultamos el mensaje
          // appGeneral.ocultarMensajeBloqueante();

          // ocultamos el modal
          $modal_cuenta_contable.modal("hide");
        });
      },

      /**
       * Establecer el codigo del item
       *
       * @private
       * @param  {object} li
       * @return {void}
       */
      establecerCodigoItem = (li) => {
        var
            $li = $(li),
            codigo;

        codigo = $li.data("orden");

        $li.parents("li").each((index, element) => {
          var $li = $(element);

          codigo = $li.data("orden") + "." + codigo;
        });

        // mostramos el codigo
        $li.find(".codigo-item").eq(0).html(codigo);

        // guardamos el codigo
        $li.data("codigo", codigo);
      },

      /**
       * Editar el item solicitado
       *
       * @private
       * @param  {object} li
       * @param  {string} nombre
       * @param  {string} descripcion
       * @return {void}
       */
      editarItem = (li, nombre, descripcion) => {
        var $li = $(li);

        $.ajax({
          url         : "/cuenta_contable/editar/" + $li.data("id"),
          async       : true,
          cache       : false,
          type        : "PATCH",
          data        : {
            nombre      : nombre,
            descripcion : descripcion,
            "_token"    : $('meta[name="csrf-token"]').attr("content")
          },
          contentType : "application/x-www-form-urlencoded",
          timeout     : TIMEOUT_AJAX,
          dataType    : "json",
          beforeSend  : function(jqXHR, settings) {
            // // mostramos el mensaje
            // appGeneral.mostrarMensajeBloqueante("Guardando Cuenta Contable");
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          if( textStatus !== "abort" ) {
            $.toast({
              heading: "Error",
              text: "Ocurrió un error al guardar la Cuenta Contable. Inténtelo más tarde.",
              icon: "error",
              position: "top-center",
              stack: 1,
              bgColor: "#ff4136",
              textColor: "white"
            });
          }
        })
        .done(function(data, textStatus, jqXHR) {
          // log( data );return;

          // establecemos el nombre
          $li.find(".nombre-item").eq(0).text(nombre);

          // establecemos la descripcion
          $li.data("descripcion", descripcion);
        })
        .always(function() {
          // // ocultamos el mensaje
          // appGeneral.ocultarMensajeBloqueante();

          // ocultamos el modal
          $modal_cuenta_contable.modal("hide");
        });
      },

      /**
       * Eliminar el item solicitado
       *
       * @private
       * @param  {object} li
       * @return {void}
       */
      eliminarItem = (li) => {
        var
            $li       = $(li),
            $li_padre = $li.parents("li").eq(0);

        // si se debe eliminar
        if( window.confirm("¿Eliminar la Cuenta Contable?") ) {
          $.ajax({
            url         : "/cuenta_contable/eliminar/" + $li.data("id"),
            async       : true,
            cache       : false,
            type        : "DELETE",
            data        : {
              "_token"    : $('meta[name="csrf-token"]').attr("content")
            },
            contentType : "application/x-www-form-urlencoded",
            timeout     : TIMEOUT_AJAX,
            dataType    : "json",
            beforeSend  : function(jqXHR, settings) {
              // // mostramos el mensaje
              // appGeneral.mostrarMensajeBloqueante("Eliminando la Cuenta Contable");
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            if( textStatus !== "abort" ) {
              $.toast({
                heading: "Error",
                text: "Ocurrió un error al eliminar la Cuenta Contable. Inténtelo más tarde.",
                icon: "error",
                position: "top-center",
                stack: 1,
                bgColor: "#ff4136",
                textColor: "white"
              });
            }
          })
          .done(function(data, textStatus, jqXHR) {
            // log( data );return;

            $li.remove();

            // ordenarItemsHijos($li_padre[0]);

            // si el item padre ya no tiene items hijos
            if( $li_padre.find("li").length === 0 ) {
              // eliminamos el boton para contraer/expandir
              $li_padre.find(".btn-contraer-expandir").html("&nbsp;");
            }
          })
          .always(function() {
            // // ocultamos el mensaje
            // appGeneral.ocultarMensajeBloqueante();
          });
        }
      },

      /**
       * Ordenar los items hijos del item solicitado
       *
       * @private
       * @param  {object} li
       * @return {void}
       */
      ordenarItemsHijos = (li) => {
        var
            $li    = $(li),
            $hijos = $li.find("> ul").find("> .item");

        // si el item posee items hijos
        if( $hijos.length ) {
          // recorremos los items hijos
          $hijos.each((index, element) => {
            var
                $li_aux    = $(element),
                $hijos_aux = $li_aux.find("> ul").find("> .item");

            // establecemos el orden del item
            $li_aux.data("orden", (index + 1));

            establecerCodigoItem($li_aux[0]);

            if( $hijos_aux.length ) {
              ordenarItemsHijos($li_aux[0]);
            }
          });
        }
      },

      /**
       * Contraer/expandir el contenido del item solicitado
       *
       * @private
       * @param  {object} li
       * @return {void}
       */
      contraerExpandir = (li) => {
        var
            $li  = $(li),
            $btn = $li.find(".btn-contraer-expandir").eq(0);

        // si esta expandido
        if( $btn.text() === "(-)" ) {
          $btn.text("(+)");

          $li.find("ul").eq(0).addClass("d-none");
        }
        // esta contraido
        else {
          $btn.text("(-)");

          $li.find("ul").eq(0).removeClass("d-none");
        }
      };


      //---------------------------------
      // PUBLICO
      //---------------------------------

  /**
   * Inicializar al app
   *
   * @public
   * @return {void}
   */
  context.inicializar = function() {
    // si no se ha inicializado la app
    if( !inicializado ) {
      acomodarItems();

      // fired when the modal has been made visible to the user
      $modal_cuenta_contable.on("shown.bs.modal", function (e) {
        // dar el foco al campo
        $input_nombre_cuenta_contable.select();
      });

      // al presionar el boton para agregar un item hijo
      $arbol.on("click", ".btn-agregar-item", function(e) {
        var $btn = $(e.currentTarget);

        li_aux_agregar_editar = $btn.parents("li").eq(0)[0];

        operacion = "agregar";

        // reiniciamos el campo
        $input_nombre_cuenta_contable
          .val("")
          .removeClass("is-invalid");

        $input_descripcion_cuenta_contable
          .val("");

        // establecemos el titulo del modal
        $titulo_modal.text("Agregar una Cuenta Contable a " + $(li_aux_agregar_editar).find(".nombre-item").eq(0).text());

        // habilitamos los botones
        $btn_guardar_modal.prop("disabled", false);
        $btn_cancelar_modal.prop("disabled", false);

        // hacemos visible el modal
        $modal_cuenta_contable.modal({
          backdrop: "static",
          keyboard: false,
          show: true
        });
      });

      // al presionar el boton para editar un item
      $arbol.on("click", ".btn-editar-item", function(e) {
        var $btn = $(e.currentTarget);

        li_aux_agregar_editar = $btn.parents("li").eq(0)[0];

        operacion = "editar";

        // establecemos el titulo del modal
        $titulo_modal.text("Editar " + $(li_aux_agregar_editar).find(".nombre-item").eq(0).text());

        // reiniciamos el campo y establecemos el nombre
        $input_nombre_cuenta_contable
          .val($btn.parents(".contenido-item").eq(0).find(".nombre-item").eq(0).text())
          .removeClass("is-invalid");

        $input_descripcion_cuenta_contable.val($btn.parents("li").data("descripcion"));

        // habilitamos los botones
        $btn_guardar_modal.prop("disabled", false);
        $btn_cancelar_modal.prop("disabled", false);

        // hacemos visible el modal
        $modal_cuenta_contable.modal({
          backdrop: "static",
          keyboard: false,
          show: true
        });
      });

      // guardamos la cuenta contable
      $form_cuenta_contable.on("submit", function(e) {
        var
            nombre      = $.trim($input_nombre_cuenta_contable.val().toUpperCase()),
            descripcion = $.trim($input_descripcion_cuenta_contable.val());

        // detenemos el envio del formulario
        e.preventDefault();

        if( nombre === "" ) {
          $input_nombre_cuenta_contable.addClass("is-invalid");
        }
        else {
          // deshabilitamos los botones
          $btn_guardar_modal.prop("disabled", true);
          $btn_cancelar_modal.prop("disabled", true);

          if( operacion == "agregar" ) {
            agregarItem(li_aux_agregar_editar, nombre, descripcion);
          } else {
            editarItem(li_aux_agregar_editar, nombre, descripcion);
          }
        }
      });

      // al presionar el boton para eliminar un item
      $arbol.on("click", ".btn-eliminar-item", function(e) {
        var $btn = $(e.currentTarget);

        eliminarItem($btn.parents("li").eq(0)[0]);
      });

      // cada vez que se presiona el boton para expandir/contraer
      $arbol.on("click", ".btn-contraer-expandir", function(e) {
        var $btn = $(e.currentTarget);

        // si el item tiene items hijos
        if( $.trim($btn.text()).length ) {
          contraerExpandir($btn.parents("li").eq(0)[0]);
        }
      });

      inicializado = true;
    }
    else {
      throw new Error("La app ya ha sido inicializada");
    }
  };

})( appPlanCuentas, window );


// DOM ready!
$(() => {

  appPlanCuentas.inicializar();

});// fin document ready