@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>Asientos Contables</h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->


<div class="row justify-content-center">
  <div class="col-md-10">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="80px">Activo</th>
          <th width="130px">Fecha</th>
          <th>Número</th>
          <th>Total</th>
          <th width="150px">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        @if ( $asientos->count() )
          @foreach ( $asientos as $asiento )
          <tr>
            <td align="center">
              {{ $asiento->deleted_at === NULL ? 'SI' : 'NO' }}
            </td>
            <td align="center">
              {{ formatoFecha($asiento->fecha) }}
            </td>
            <td>{{ str_pad($asiento->numero, 8, '0', STR_PAD_LEFT) }}</td>
            <td>{{ formatoNumero($asiento->total) }}</td>
            <td align="center">
              <div class="info" data-id="{{ $asiento->id }}"></div>
              <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary btn-editar" title="ir a la edición del asiento">
                  Editar
                </button>
                @if( $asiento->deleted_at === NULL )
                <button type="button" class="btn btn-danger btn-eliminar" title="eliminar el asiento">
                  Eliminar
                </button>
                @else
                <button type="button" class="btn btn-success btn-restaurar" title="restaurar el asiento">
                  Restaurar
                </button>
                @endif
              </div>
            </td>
          </tr>
          @endforeach
        @else
        <tr>
          <td colspan="5" align="center">No hay asientos cargados</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-md-10">
    {{ $asientos->links() }}
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/asiento_contable/listado.js') }}"></script>
@endsection