@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>
          {{ $asiento->id == 0 ? 'Cargar' : 'Editar' }} Asiento Contable <small>N&deg; {{ $asiento->numero }}</small>
        </h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->



<div class="row justify-content-center">
  <div class="col-md-8">

    <div class="shadow-sm p-3 bg-white rounded">
      <form action="{{ $asiento->id == 0 ? '/asiento_contable/guardar' : '/asiento_contable/editar/' . $asiento->id }}" method="POST" id="formulario-asiento-contable">
        <input type="hidden" name="total" id="total" />
        @csrf

        @if ( $asiento->id )
          @method('PATCH')
        @endif

        <div class="row">
          <div class="col-2">
            <div class="form-group">
              <label for="fecha">Fecha</label>
              <input type="text" name="fecha" id="fecha" value="{{ old('fecha', $asiento->fecha) }}" class="form-control text-center @error('fecha') is-invalid @enderror" autocomplete="off" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar una fecha válida.</strong>
              </span>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="debe">Debe</label>
              <input type="text" id="debe" value="0" class="form-control text-right" autocomplete="off" readonly />
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="haber">Haber</label>
              <input type="text" id="haber" value="0" class="form-control text-right" autocomplete="off" readonly />
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="diferencia">Diferencia</label>
              <input type="text" id="diferencia" value="0" class="form-control text-right" autocomplete="off" readonly />

              <span class="invalid-feedback" role="alert">
                <strong>La diferencia entre el Debe y el Haber debe ser igual a 0.</strong>
              </span>
            </div>
          </div>
        </div><!-- /row -->


        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="observaciones">Observaciones</label>
              <textarea name="observaciones" id="observaciones" class="form-control" rows="3">{{ old('observaciones', $asiento->observaciones) }}</textarea>
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <button type="button" id="btn-agregar-item" class="btn btn-secondary btn-sm" title="agregar un nuevo item al asiento">+ Item</button>

            <table class="table mt-2" id="tabla-items">
              <thead>
                <tr>
                  <th>Cuenta Contable</th>
                  <th width="190px">Monto</th>
                  <th width="130px">Tipo</th>
                  <th width="80px">&nbsp;</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col text-center">
            <button type="submit" class="btn btn-primary" id="btn-guardar">Guardar</button>
            <button type="button" class="btn btn-secondary" id="btn-listado">Volver al listado</button>
          </div>
        </div><!-- /row -->
      </form>
    </div>

  </div>
</div><!-- /.row -->


<script id="fila-item-template" type="text/x-handlebars-template">
<tr class="fila-item">
  <td>
    <select class="form-control form-control-sm input-cuenta-contable"></select>
  </td>
  <td>
    <input type="text" class="form-control form-control-sm text-right input-monto" value="0" />
    <input type="hidden" class="input-monto-oculto" />
  </td>
  <td>
    <select class="form-control form-control-sm input-tipo">
      <option value="DEBE">DEBE</option>
      <option value="HABER">HABER</option>
    </select>
  </td>
  <td class="text-center">
    <button type="button" class="btn btn-danger btn-sm btn-eliminar" title="eliminar item">
      x
    </button>
  </td>
</tr>
</script>
@endsection

@section('styles')
<link href="{{ asset('css/vendor/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/vendor/select2.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/vendor/handlebars.min.js') }}"></script>
<script src="{{ asset('js/vendor/autoNumeric.min.js') }}"></script>
<script src="{{ asset('js/vendor/select2.min.js') }}"></script>
<script src="{{ asset('js/vendor/select2-es.js') }}"></script>
<script src="{{ asset('js/asiento_contable/index.js') }}"></script>
<script>
const items = @json($items);
</script>
@endsection