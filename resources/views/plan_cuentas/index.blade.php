@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>
          Plan de Cuentas <small>{{ Session::get('empresa') }}</small>
        </h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->

<div class="row justify-content-center">
  <div class="col-md-8">

    <div class="shadow-sm p-3 bg-white rounded">
      <ul id="arbol">
        <li id="item1" class="item" data-id="{{ $activo->id }}" data-orden="{{ $activo->orden }}" data-tipo="{{ $activo->tipo }}">
          <div class="contenido-item">
            <div class="btn-contraer-expandir no-select">(-)</div>
            <div class="codigo-item">1</div>
            <div class="nombre-item"><strong>ACTIVO</strong></div>
            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                +
              </button>
            </div>
          </div>
          {!! $arbol_activo !!}
        </li>
        <li id="item1" class="item" data-id="<?php echo $pasivo->id ?>" data-orden="<?php echo $pasivo->orden ?>" data-tipo="<?php echo $pasivo->tipo ?>">
          <div class="contenido-item">
            <div class="btn-contraer-expandir no-select">(-)</div>
            <div class="codigo-item">2</div>
            <div class="nombre-item"><strong>PASIVO</strong></div>
            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                +
              </button>
            </div>
          </div>
          {!! $arbol_pasivo !!}
        </li>
        <li id="item1" class="item" data-id="{{ $patrimonio_neto->id }}" data-orden="{{ $patrimonio_neto->orden }}" data-tipo="{{ $patrimonio_neto->tipo }}">
          <div class="contenido-item">
            <div class="btn-contraer-expandir no-select">(-)</div>
            <div class="codigo-item">3</div>
            <div class="nombre-item"><strong>PATRIMONIO NETO</strong></div>
            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                +
              </button>
            </div>
          </div>
          {!! $arbol_patrimonio_neto !!}
        </li>
        <li id="item1" class="item" data-id="{{ $ingresos->id }}" data-orden="{{ $ingresos->orden }}" data-tipo="{{ $ingresos->tipo }}">
          <div class="contenido-item">
            <div class="btn-contraer-expandir no-select">(-)</div>
            <div class="codigo-item">4</div>
            <div class="nombre-item"><strong>INGRESOS</strong></div>
            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                +
              </button>
            </div>
          </div>
          {!! $arbol_ingresos !!}
        </li>
        <li id="item1" class="item" data-id="{{ $egresos->id }}" data-orden="{{ $egresos->orden }}" data-tipo="{{ $egresos->tipo }}">
          <div class="contenido-item">
            <div class="btn-contraer-expandir no-select">(-)</div>
            <div class="codigo-item">5</div>
            <div class="nombre-item"><strong>EGRESOS</strong></div>
            <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
              <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
                +
              </button>
            </div>
          </div>
          {!! $arbol_egresos !!}
        </li>
      </ul>
    </div>

  </div>
</div><!-- /.row -->

<!-- template del item -->
<script id="item-template" type="text/x-handlebars-template">
  <div class="contenido-item">
    <div class="btn-contraer-expandir no-select">&nbsp;</div>
    <div class="codigo-item"></div>
    <div class="nombre-item">@{{nombre}}</div>
    <div class="btn-group btn-group-sm btns-item" role="group" aria-label="...">
      <button type="button" class="btn btn-primary btn-agregar-item" title="agregar una cuenta contable">
        +
      </button>
      <button type="button" class="btn btn-secondary btn-editar-item" title="editar la cuenta contable">
        editar
      </button>
      <button type="button" class="btn btn-danger btn-eliminar-item" title="eliminar la cuenta contable y si los tuviera, sus descendientes">
        x
      </button>
    </div>
  </div>
</script>

<!-- modal del item -->
<div id="modal-cuenta-contable" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="titulo-modal" class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="form-cuenta-contable">
          <table width="100%" border="0">
            <tr>
              <td>
                <div class="form-group">
                  <label class="control-label" for="input-nombre-cuenta-contable">Nombre:</label>
                  <input type="text" id="input-nombre-cuenta-contable" maxlength="200" placeholder="Nombre" class="form-control input-erp">

                  <span class="invalid-feedback" role="alert">
                    <strong>Debe ingresar el Nombre.</strong>
                  </span>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="form-group">
                  <label class="control-label" for="input-descripcion-cuenta-contable">Descripción:</label>
                  <textarea id="input-descripcion-cuenta-contable" placeholder="Descripción" class="form-control input-erp" rows="3" style="resize: none;"></textarea>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="text-center">
                  <button type="submit" id="btn-guardar-modal" class="btn btn-sm btn-primary">Guardar</button>
                  <button type="button" id="btn-cancelar-modal" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection


@section('styles')
<link href="{{ asset('css/empresa/plan_cuentas.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/vendor/handlebars.min.js') }}"></script>
<script src="{{ asset('js/empresa/plan_cuentas.js') }}"></script>
@endsection