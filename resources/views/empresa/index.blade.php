@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>
          {{ $empresa->id == 0 ? 'Cargar' : 'Editar' }} Empresa
        </h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->

<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="shadow-sm p-3 bg-white rounded">
      <form action="{{ $empresa->id == 0 ? '/empresa/guardar' : '/empresa/editar/' . $empresa->id }}" method="POST" id="formulario-empresa">
        @csrf

        @if ( $empresa->id )
          @method('PATCH')
        @endif

        <div class="row">
          <div class="col-2">
            <div class="form-group">
              <label for="inicio_actividad">Inicio actividad</label>
              <input type="text" name="inicio_actividad" id="inicio_actividad" value="{{ old('inicio_actividad', $empresa->inicio_actividad) }}" class="form-control text-center @error('inicio_actividad') is-invalid @enderror" autocomplete="off" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar una fecha válida.</strong>
              </span>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="razon_social">Razón social</label>
              <input type="text" name="razon_social" id="razon_social" value="{{ old('razon_social', $empresa->razon_social) }}" class="form-control @error('razon_social') is-invalid @enderror" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar la Razón social.</strong>
              </span>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="nombre_fantasia">Nombre fantasía</label>
              <input type="text" name="nombre_fantasia" id="nombre_fantasia" value="{{ old('nombre_fantasia', $empresa->nombre_fantasia) }}" class="form-control @error('nombre_fantasia') is-invalid @enderror" />

              @error('nombre_fantasia')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="direccion">Dirección</label>
              <input type="text" name="direccion" id="direccion" value="{{ old('direccion', $empresa->direccion) }}" class="form-control @error('direccion') is-invalid @enderror" />

               @error('direccion')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="telefono">Teléfono</label>
              <input type="text" name="telefono" id="telefono" value="{{ old('telefono', $empresa->telefono) }}" class="form-control @error('telefono') is-invalid @enderror" />

               @error('telefono')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label for="email">E-mail</label>
              <input type="text" name="email" id="email" value="{{ old('email', $empresa->email) }}" class="form-control @error('email') is-invalid @enderror" />

               @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="observaciones">Observaciones</label>
              <textarea name="observaciones" id="observaciones" class="form-control @error('observaciones') is-invalid @enderror">{{ old('observaciones', $empresa->observaciones) }}</textarea>

              @error('observaciones')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col text-center">
            <button type="submit" class="btn btn-primary" id="btn-guardar">Guardar</button>
            <button type="button" class="btn btn-secondary" id="btn-listado">Volver al listado</button>
          </div>
        </div><!-- /row -->
      </form>
    </div>
  </div>
</div><!-- /.row -->
@endsection

@section('styles')
<link href="{{ asset('css/vendor/jquery-ui.min.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/empresa/index.js') }}"></script>
@endsection