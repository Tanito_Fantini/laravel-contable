@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>Empresas</h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->


<div class="row justify-content-center">
  <div class="col-md-10">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="80px">Activo</th>
          <th width="130px">Inicio actividad</th>
          <th>Razón Social</th>
          <th width="150px">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        @if ( $empresas->count() )
          @foreach ( $empresas as $empresa )
          <tr>
            <td align="center">
              {{ $empresa->deleted_at === NULL ? 'SI' : 'NO' }}
            </td>
            <td align="center">
              {{ formatoFecha($empresa->inicio_actividad) }}
            </td>
            <td>{{ $empresa->razon_social }}</td>
            <td align="center">
              <div class="info" data-id="{{ $empresa->id }}"></div>
              <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary btn-editar" title="ir a la edición de la empresa">
                  Editar
                </button>
                @if( $empresa->deleted_at === NULL )
                <button type="button" class="btn btn-danger btn-eliminar" title="eliminar la empresa">
                  Eliminar
                </button>
                @else
                <button type="button" class="btn btn-success btn-restaurar" title="restaurar la empresa">
                  Restaurar
                </button>
                @endif
              </div>
            </td>
          </tr>
          @endforeach
        @else
        <tr>
          <td colspan="4" align="center">No hay empresas cargadas</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-md-10">
    {{ $empresas->links() }}
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/empresa/listado.js') }}"></script>
@endsection