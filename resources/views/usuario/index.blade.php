@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>
          {{ $usuario->id == 0 ? 'Cargar' : 'Editar' }} Usuario <small>{{ $usuario->name }}</small>
        </h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->

<div class="row justify-content-center">
  <div class="col-md-4">

    <div class="shadow-sm p-3 bg-white rounded">
      <form action="{{ $usuario->id == 0 ? '/usuario/guardar' : '/usuario/editar/' . $usuario->id }}" method="POST" id="formulario-usuario">
        <input type="hidden" id="id" value="{{ $usuario->id }}" />
        @csrf

        @if ( $usuario->id )
          @method('PATCH')
        @endif

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="email">E-mail</label>
              <input type="text" name="email" id="email" value="{{ old('email', $usuario->email) }}" class="form-control @error('email') is-invalid @enderror" autocomplete="off" maxlength="255" @if( $usuario->id ) readonly @endif />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar un E-mail.</strong>
              </span>
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" name="name" id="nombre" value="{{ old('nombre', $usuario->name) }}" class="form-control @error('nombre') is-invalid @enderror" autocomplete="off" maxlength="255" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar un Nombre.</strong>
              </span>
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="contrasenia">
                Contraseña
                <span data-toggle="tooltip" data-placement="top" title="Debe ser de al menos 8 caracteres de longitud">(!)</span>
              </label>
              <input type="password" name="password" id="contrasenia" class="form-control @error('contrasenia') is-invalid @enderror" autocomplete="off" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar una Contraseña.</strong>
              </span>
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="confirmar_contrasenia">Confirmar Contraseña</label>
              <input type="password" name="password_confirmation" id="confirmar_contrasenia" class="form-control @error('confirmar_contrasenia') is-invalid @enderror" autocomplete="off" />

              <span class="invalid-feedback" role="alert">
                <strong>Debe ingresar la Confirmación de la Contraseña.</strong>
              </span>
            </div>
          </div>
        </div><!-- /row -->

        <div class="row">
          <div class="col text-center">
            <button type="submit" class="btn btn-primary" id="btn-guardar">Guardar</button>
            <button type="button" class="btn btn-secondary" id="btn-listado">Volver al listado</button>
          </div>
        </div><!-- /row -->
      </form>
    </div>

  </div>
</div><!-- /.row -->
@endsection

@section('scripts')
<script src="{{ asset('js/usuario/index.js') }}"></script>
@endsection