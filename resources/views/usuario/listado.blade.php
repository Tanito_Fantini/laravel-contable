@extends('layouts.main')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-12">
    <fieldset>
      <legend>
        <h3>Usuarios</h3>
      </legend>
    </fieldset>
  </div>
</div><!-- /.row -->


<div class="row justify-content-center">
  <div class="col-md-10">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th width="80px">Activo</th>
          <th>Nombre</th>
          <th>E-mail</th>
          <th width="150px">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        @if ( $usuarios->count() )
          @foreach ( $usuarios as $usuario )
          <tr>
            <td align="center">
              {{ $usuario->deleted_at === NULL ? 'SI' : 'NO' }}
            </td>
            <td>
              {{ $usuario->name }}
            </td>
            <td>
              {{ $usuario->email }}
            </td>
            <td align="center">
              @if( $usuario->id > 1 )
              <div class="info" data-id="{{ $usuario->id }}"></div>
              <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary btn-editar" title="ir a la edición del usuario">
                  Editar
                </button>
                @if( $usuario->deleted_at === NULL )
                <button type="button" class="btn btn-danger btn-eliminar" title="eliminar el usuario">
                  Eliminar
                </button>
                @else
                <button type="button" class="btn btn-success btn-restaurar" title="restaurar el usuario">
                  Restaurar
                </button>
                @endif
              </div>
              @endif
            </td>
          </tr>
          @endforeach
        @else
        <tr>
          <td colspan="4" align="center">No hay usuarios cargados</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-md-10">
    {{ $usuarios->links() }}
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/usuario/listado.js') }}"></script>
@endsection