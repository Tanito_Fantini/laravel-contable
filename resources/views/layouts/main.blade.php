<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')
  </head>
  <body>
    @include('includes/nav')

    <div class="container-fluid mt-3">
      @if( $errors->any() )
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
          {{ $error }}
        </div>
        @endforeach
      @endif

      @if( Session::has('error') )
      <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
      </div>
      @endif

      @yield('content')
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-seleccion-empresas" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Seleccione la Empresa de trabajo</h5>

            <img src="{{ asset('images/loading.gif') }}" alt="" width="15px" id="img-obteniendo-empresas" class="ml-2 mt-2 d-none" />

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table id="tabla-seleccionar-empresa" class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <tr>
                  <th width="110px">Inicio <abbr title="Actividad">Act.</abbr></th>
                  <th>Razón Social</th>
                  <th width="100px">&nbsp;</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')
  </body>
</html>