CREATE USER 'laravel_demo_user'@'localhost' IDENTIFIED WITH mysql_native_password AS '***';
GRANT USAGE ON *.* TO 'laravel_demo_user'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0
MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILLEGES ON `laravel_demo_database`.* TO 'laravel_demo_user'@'localhost';